#### This project is for the GitLab CI/CD

##### Test

The project uses jest library for tests. (see "test" script in package.json)
There is 1 test (server.test.js) in the project that checks whether the main index.html file exists in the project.

To run the nodejs test:

    npm run test

Make sure to download jest library before running test, otherwise jest command defined in package.json won't be found.

    npm install

In order to see failing test, remove index.html or rename it and run tests.

# Starting from chapter 2:

Simple demo Node.js project: <https://gitlab.com/nanuchi/mynodeapp-cicd-project>

# Starting from chapter 7 (Microservices):

Microservice mono-repo: <https://gitlab.com/nanuchi/mymicroservice-cicd>
Microservice poly-repo: <https://gitlab.com/mymicroservice-cicd>
CI-templates (in the poly-repo group): <https://gitlab.com/mymicroservice-cicd/ci-templates>
